#include "Dathuc.h"


void themCuoiBienSo(BienSo* &head, char bienso, unsigned int somu)
{
	BienSo* newNode;
	BienSo* nodePtr;
	newNode = new BienSo;
	newNode->bien = bienso;
	newNode->soMu = somu;
	newNode->next = NULL;
	if (!head)
	{
		head = newNode;
	}
	else {
		nodePtr = head;
		while (nodePtr->next)
			nodePtr = nodePtr->next;
		nodePtr->next = newNode;
	}

}
void themCuoiDaThuc(DaThuc* &phead, DonThuc x1)
{
	DaThuc* newNode;
	DaThuc* nodePtr;
	newNode = new DaThuc;
	newNode->data.a = x1.a;
	BienSo*cur = x1.headDonThuc;
	while (cur != NULL)
	{
		themCuoiBienSo(newNode->data.headDonThuc, cur->bien, cur->soMu);
		cur = cur->next;
	}
	newNode->next = NULL;
	if (!phead)
	{
		phead = newNode;
	}
	else {
		nodePtr = phead;
		while (nodePtr->next)
			nodePtr = nodePtr->next;
		nodePtr->next = newNode;
	}

}
void xoaListBienSo(BienSo* &pHead)
{

	BienSo* NextNode;

	while (pHead != NULL)
	{
		NextNode = pHead->next;
		delete pHead;
		pHead = NextNode;
	}
}
void xoaListDaThuc(DaThuc* &pHead)
{

	DaThuc* NextNode;
	pHead = pHead;

	while (pHead != NULL)
	{
		NextNode = pHead->next;
		xoaListBienSo(pHead->data.headDonThuc);
		delete pHead;
		pHead = NextNode;
	}
}
bool soSanhDonThuc(DonThuc a, DonThuc b)
{
	BienSo *l = a.headDonThuc;
	BienSo *r = b.headDonThuc;
	while (l != NULL&&r != NULL)
	{
		if (r->soMu != l->soMu || r->bien != l->bien)
		{
			return false;
		}
		l = l->next;
		r = r->next;
	}
	if (l == NULL&&r == NULL)
		return true;
	else
		return false;
}
bool soSanhDonThuc(DaThuc* a, DaThuc* b)
{
	BienSo* l = a->data.headDonThuc;
	BienSo*r = b->data.headDonThuc;

	while (l != NULL&&r != NULL)
	{
		if (r->soMu != l->soMu || r->bien != l->bien)
		{
			return false;
		}
		l = r->next;
	}
	return true;
}
