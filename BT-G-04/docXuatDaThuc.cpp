#include "Dathuc.h"


bool laSoAm = false;
bool chuahetdong = true;
bool laSoAm2 = false;
bool khongCoHeSo = false;

void docDonThuc(fstream &filein, DonThuc &donthuctam)
{
	char temp = NULL;
	bool test = false;

	filein >> donthuctam.a;  // doc he so
	if (laSoAm == true)
	{
		donthuctam.a *= -1;
		laSoAm = false;
	}
	while (1) // doc tung don thuc 
	{
		string s;
		if (temp == '+')
		{
			break;
		}
		if (temp == '-')
		{
			laSoAm = true;
			break;
		}
		if (temp == '@')
		{
			chuahetdong = false;
			break;

		}
		filein >> temp;
		if (temp == '^')
			test = true;
		if (temp != '*'&&temp != '^')
		{
			if (temp >= 'a'&&temp <= 'z' || temp >= 'A'&&temp <= 'Z')
			{
				unsigned int somu = 1;
				themCuoiBienSo(donthuctam.headDonThuc, temp, somu);
			}
			while (temp >= '0' && temp <= '9')
			{
				s = s + temp;
				filein >> temp;
			}
			if (test == true)
			{

				BienSo*cur = donthuctam.headDonThuc;
				while (cur->next != NULL)
					cur = cur->next;

				cur->soMu = atoi(s.c_str());
				s = "";
				test = false;
			}
		}

	}
}
void docDaThucTuFile(fstream &filein, DaThuc* &head)
{

	DonThuc donthuctam;
	while (chuahetdong) // doc het da thuc
	{
		docDonThuc(filein, donthuctam);
		themCuoiDaThuc(head, donthuctam);
		xoaListBienSo(donthuctam.headDonThuc);
		donthuctam.a = NULL;
		donthuctam.headDonThuc = NULL;
	}
	chuahetdong = true;
}
void vietDonThuc(fstream &fileout, DonThuc donthuc)
{
	BienSo* cur = donthuc.headDonThuc;
	fileout << donthuc.a;
	while (cur != NULL)
	{
		fileout << '*';
		fileout << cur->bien;
		if (cur->soMu != 1)
		{
			fileout << '^';
			fileout << cur->soMu;
		}
		cur = cur->next;
	}

}
void vietDaThucVaoFile(fstream &fileout, DaThuc *dathuc)
{
	DaThuc*cur = dathuc;
	DonThuc donthuc;
	while (cur != NULL)
	{
		//vietdonthuc
		donthuc = cur->data;
		vietDonThuc(fileout, donthuc);
		if (cur->next != NULL&&cur->next->data.a >= 0)
		{
			fileout << "+";
		}
		cur = cur->next;
	}

}