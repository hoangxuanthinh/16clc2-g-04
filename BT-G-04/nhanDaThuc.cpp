#include "Dathuc.h"

DaThuc* nhanDaThuc(DaThuc* a, DaThuc* b)
{
	DaThuc *result = NULL;

	DaThuc *curA = a;

	//Duyet da thuc a
	while (curA != NULL)
	{
		DonThuc tmp;
		DaThuc *curB = b;

		//Duyet da thuc b
		while (curB != NULL)
		{
			//Nhan he so
			tmp.a = curA->data.a*curB->data.a;

			BienSo *curBiena = curA->data.headDonThuc;
			BienSo *curBienb = curB->data.headDonThuc;
			//Nhan bien so
			while (curBiena != NULL)
			{

				while (curBienb != NULL)
				{
					if (curBiena->bien < curBienb->bien)
						themCuoiBienSo(tmp.headDonThuc, curBiena->bien, curBiena->soMu);
					else if (curBiena->bien == curBienb->bien)
						themCuoiBienSo(tmp.headDonThuc, curBiena->bien, curBiena->soMu + curBienb->soMu);
					else
						themCuoiBienSo(tmp.headDonThuc, curBienb->bien, curBienb->soMu);
					curBienb = curBienb->next;
				}
				curBiena = curBiena->next;
			}
			//Add don thuc vao da thuc
			themCuoiDaThuc(result, tmp);
			curB = curB->next;
		}
		curA = curA->next;
	}
	return result;
}