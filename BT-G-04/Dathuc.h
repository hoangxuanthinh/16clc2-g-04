#pragma once
#include <iostream>
#include <string>
#include <cstring>
#include <cmath>
#include <fstream>

using namespace std;


struct BienSo
{
	char bien;
	int soMu;
	BienSo *next;

};

// Tao mot cau truc don thuc 
struct DonThuc
{
	float a; // a la he so cua moi don thuc
	BienSo *headDonThuc = NULL; // danh sach lien ket chua cac bien cua don thuc
};

// Tao danh sach lien ket chua cac don thuc de tao thanh mot da thuc
struct DaThuc
{
	DonThuc data; // chua cac du lieu cua mot don thuc
	DaThuc* next; // tro toi Node tiep theo
};

void themCuoiBienSo(BienSo* &head, char bienso, unsigned int somu);
void themCuoiDaThuc(DaThuc* &phead, DonThuc x1);
void docDonThuc(fstream &filein, DonThuc &donthuctam);
void vietDonThuc(fstream &fileout, DonThuc donthuc);
void vietDaThucVaoFile(fstream &fileout, DaThuc *dathuc);
bool soSanhDonThuc(DaThuc* a, DaThuc* b);
bool soSanhDonThuc(DonThuc a, DonThuc b);
bool soSanhDonThuc1(DaThuc *d1, DaThuc *d2);
void chuanHoa(DaThuc *&head);
void rutGon(DaThuc* &pHead);
void xoaListBienSo(BienSo* &pHead);
void xoaListDaThuc(DaThuc* &pHead);
void docDaThucTuFile(fstream &filein, DaThuc* &head);
DaThuc* congDaThuc(DaThuc* dathuc1, DaThuc* dathuc2);
DaThuc* truDaThuc(DaThuc* dathuc1, DaThuc* dathuc2);
DaThuc* nhanDaThuc(DaThuc* a, DaThuc* b);