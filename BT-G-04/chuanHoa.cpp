#include "Dathuc.h"


bool soSanhDonThuc1(DaThuc *d1, DaThuc *d2)
{
	bool smaller = true;
	BienSo *cur1 = d1->data.headDonThuc;
	BienSo *cur2 = d2->data.headDonThuc;
	while (cur1 != NULL && cur2 != NULL)	//duyet list bien so
	{
		if (cur1->next != NULL || cur2->next != NULL)
			if (cur1->soMu > cur2->soMu)	//so sanh so mu va so sanh ma ASCII cua bien
				smaller = false;
			else
				if (cur1->soMu == cur2->soMu && int(cur1->bien) > int(cur2->bien))
					smaller = false;
		if (cur1->soMu > cur2->soMu)	//so sanh so mu va so sanh ma ASCII cua bien
			smaller = false;
		else
			if (cur1->soMu == cur2->soMu && int(cur1->bien) > int(cur2->bien))
				smaller = false;
		cur1 = cur1->next;
		cur2 = cur2->next;
	}
	if (cur1 == NULL && cur2 != NULL)
		smaller = true;
	else
		if (cur2 == NULL && cur1 != NULL)
			smaller = false;
		else
			if (d1->data.a < d2->data.a && smaller == true)	//so sanh he so
				smaller = true;
	return smaller;
}

void chuanHoa(DaThuc *&head)
{
	DaThuc *i, *j;

	//loop #1
	for (i = head; i->next != NULL; i = i->next)
	{
		DaThuc *min = i;
		//loop #2
		for (j = i->next; j != NULL; j = j->next)
			if (soSanhDonThuc1(j, min) == true)
				min = j;

		if (min != i)
		{
			DonThuc tmp;
			tmp = min->data;
			min->data = i->data;
			i->data = tmp;
		}
	}
}